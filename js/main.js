const getApp = () => {
  window.open("https://www.google.by/");
};

const watchVideo = () => {
  window.open("https://www.youtube.com/");
};

const sliderArray = [
  {
    src: "./assets/Screen 1.png",
  },
  {
    src: "./assets/Screen 2.png",
  },
  {
    src: "./assets/Screen 3.png",
  },
];

const moveRight = () => {
  const leftScreen = document.getElementById("leftScreen");
  const middleScreen = document.getElementById("middleScreen");
  const rightScreen = document.getElementById("rightScreen");
  leftScreen.src = sliderArray[2].src;
  middleScreen.src = sliderArray[0].src;
  rightScreen.src = sliderArray[1].src;
  const temp = sliderArray[0];
  sliderArray[0] = sliderArray[2];
  sliderArray[2] = sliderArray[1];
  sliderArray[1] = temp;
};

const moveLeft = () => {
  const leftScreen = document.getElementById("leftScreen");
  const middleScreen = document.getElementById("middleScreen");
  const rightScreen = document.getElementById("rightScreen");
  rightScreen.src = sliderArray[0].src;
  middleScreen.src = sliderArray[2].src;
  leftScreen.src = sliderArray[1].src;
  const temp = sliderArray[2];
  sliderArray[2] = sliderArray[0];
  sliderArray[0] = sliderArray[1];
  sliderArray[1] = temp;
};

const reviews = [
  {
    id: "icon-man-black",
    headline: "Lorem ipsum dolor sit amet.",
    text:
      '"Lorem ipsum, dolor sit amet consectetur adipisicing elit. Omnis, consectetur est. Facilis esse blanditiis quod recusandae asperiores, iusto enim pariatur a minus sit mollitia quidem id eligendi tempore soluta dolor."',
    rate: 4,
    name: "Arthur Durgan",
  },
  {
    id: "icon-woman-brown",
    headline: "Ut enim ad minim veniam.",
    text:
      '"Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur."',
    rate: 5,
    name: "Henry Walter",
  },
  {
    id: "icon-woman-pink",
    headline: "Excepteur sint occaecat cupidatat non proident, sunt in culpa.",
    text:
      '"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem."',
    rate: 4,
    name: "Brooke Krajcik",
  },
  {
    id: "icon-man-gray",
    headline: "Duis aute irure dolor in reprehenderit.",
    text: '"Maecenas nec odio et ante tincidunt tempus. Nullam quis ante."',
    rate: 3,
    name: "John Rutherford",
  },
  {
    id: "icon-woman-gray",
    headline: "Cum sociis natoque penatibus et magnis dis parturient montes.",
    text:
      '"In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus."',
    rate: 5,
    name: "Joseph Emmerich",
  },
];

const chengeStyle = (id) => {
  const removeStyle = document.getElementsByClassName(
    "reviews-area__slider--onclick"
  )[0];
  removeStyle.classList.remove("reviews-area__slider--onclick");
  const selectedIcon = document.getElementById(String(id));
  selectedIcon.classList.add("reviews-area__slider--onclick");
  const selectedObject = reviews.find((item) => id === item.id);
  if (selectedObject && selectedObject.id) {
    const headline = document.getElementsByClassName(
      "reviews-area__info--headline"
    )[0];
    const text = document.getElementsByClassName("reviews-area__info--text")[0];
    const name = document.getElementsByClassName("reviews-area__info--name")[0];
    headline.innerHTML = selectedObject.headline;
    text.innerHTML = selectedObject.text;
    name.innerHTML = selectedObject.name;
    const rate = document.getElementsByClassName("fa-star");
    for (let i = 0; i < rate.length; i++) {
      if (i + 1 <= selectedObject.rate) {
        rate[i].classList.add("checked");
      } else {
        rate[i].classList.remove("checked");
      }
    }
  }
};

const changePrice = (id) => {
  const removeStyleCard = document.getElementsByClassName(
    "price-area__card--changed-style"
  )[0];
  removeStyleCard.classList.remove("price-area__card--changed-style");
  const removeStylePlan = document.getElementsByClassName(
    "price-area__choose--button-changed-style"
  )[0];
  removeStylePlan.classList.remove("price-area__choose--button-changed-style");
  const selectedCard = document.getElementById(String(id));
  selectedCard.classList.add("price-area__card--changed-style");
  if (id === "rightCard") {
    const selectedPlan = document.getElementById("rightPlan");
    selectedPlan.classList.add("price-area__choose--button-changed-style");
  } else {
    const selectedPlan = document.getElementById("leftPlan");
    selectedPlan.classList.add("price-area__choose--button-changed-style");
  }
};

const changePlan = (id) => {
  const removeStyleCard = document.getElementsByClassName(
    "price-area__card--changed-style"
  )[0];
  removeStyleCard.classList.remove("price-area__card--changed-style");
  const removeStylePlan = document.getElementsByClassName(
    "price-area__choose--button-changed-style"
  )[0];
  removeStylePlan.classList.remove("price-area__choose--button-changed-style");
  const selectedPlan = document.getElementById(String(id));
  selectedPlan.classList.add("price-area__choose--button-changed-style");
  if (id === "rightPlan") {
    const selectedPlan = document.getElementById("rightCard");
    selectedPlan.classList.add("price-area__card--changed-style");
  } else {
    const selectedPlan = document.getElementById("leftCard");
    selectedPlan.classList.add("price-area__card--changed-style");
  }
};

const deleteEmail = () => {
  document.getElementsByClassName("email-area__input--text")[0].value = "";
}